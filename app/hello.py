#!flask/bin/python
from flask import jsonify
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return jsonify({"test": 1})

if __name__ == '__main__':
    app.run(host='0.0.0.0')
